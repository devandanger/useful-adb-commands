#Start Activity#
* adb shell am start -n ``<package>/.<activity>``
* adb shell am start -n ``<package>/<package.activity>``

#Kill Activity#
* adb shell am force-stop ``<package>``

#Install App#
* adb install ``<file name>``

#Uninstall App#
* adb uninstall ``<package>``

#Location Fix#
* telnet ``<hostname>`` ``<port>``
* geo fix ``<latitude>`` ``<longitude>``

#Broadcasting with Intent#
* adb shell am broadcast -a ``<action>``

#Broadcasting with Intent Data#
* adb shell am broadcast -a ``<action>`` -e ``<Intent Key>`` ``<Intent Value>`` ... -e ``<Intent Key>`` ``<Intent Value>``

